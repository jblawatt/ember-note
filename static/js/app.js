(function ($) {

    window.App = Ember.Application.create({
        rootElement: "#main-content-row"
    });

    // App.Router = Ember.Router.extend({});

    App.Router.map(function () {
        this.resource('pages', function () {
            this.resource('page', {path: ':page_id'});
        });
    });


    App.PagesRoute = Ember.Route.extend({
        model: function (params) {
            return App.Page.find();
        },
        setupController: function (controller, model) {
            controller.set('model', model);
        }
    });

    App.PageRoute = Ember.Route.extend({
        model: function (params) {
            return App.Page.find(params.page_id);
        },
    });


    Ember.Handlebars.helper("markdown", function (value) {
        var converter = new Showdown.converter({extensions: ["checkbox"]});
        return new Handlebars.SafeString(converter.makeHtml(value));
    });

    var currentEditor = null;

    var NoteAdapter = DS.RESTAdapter.extend({
        namespace: "api"
    });

    App.Store = DS.Store.extend({
        adapter: NoteAdapter.create({
            plurals: {
                note: "notes"
            },
            namespace: "api"
        })
    });

    App.Note = DS.Model.extend({
        markdownContent: DS.attr('string'),
        height: DS.attr('number'),
        width: DS.attr('number'),
        left: DS.attr('number'),
        top: DS.attr('number')
    });

    App.Page = DS.Model.extend({
        name: DS.attr('string')
    });

    window.NoteView = Ember.View.extend({

        templateName: "notetemplate",

        attributeBindings: ["style"],
        classNames: "note",
        classNameBindings: ["nodeSelected"],

        // load and bind the style from the controller
        style: function () {
            return "height: " + this.controller.get('height') + "px; width: " + this.controller.get('width') + "px; " +
                    "top: " + this.controller.get('top') + "px; left: " + this.controller.get('left') + "px;";
        }.property(),

        // containing button and their action bar
        actionBar: null,
        editButton: null,
        destroyButton: null,

        // event manager for this class
        // handling the event thrown on this element
        eventManager: Ember.Object.create({

            // show or hide the actionbar on
            // mouseover on the view container.
            mouseEnter: function (evt, view) {
                if (view.actionBar) view.actionBar.css({visibility: "visible"});
            },
            mouseLeave: function (evt, view) {
                if (view.actionBar) view.actionBar.css({visibility: "hidden"});
            }

        }),

        // eventhandler for begin the resizing
        onResizeStart: function (evt, ui) {
            this.disableButtons();
        },

        // eventhandler for stop the resizing
        onResizeStop: function (evt, ui) {
            this.controller.set("width", ui.size.width);
            this.controller.set("height", ui.size.height);
            this.controller.store.commit();
            this.enableButtons();
        },

        onDragStop: function (evt, ui) {
            this.controller.set("top", ui.position.top);
            this.controller.set("left", ui.position.left);
            this.controller.store.commit();

            this.enableButtons();
        },
        onDragStart: function (evt, ui) {
            this.disableButtons();
        },

        disableButtons: function () {
            this.editButton.addClass("disabled");
            this.destroyButton.addClass("disabled");
        },
        enableButtons: function () {
            this.editButton.removeClass("disabled");
            this.destroyButton.removeClass("disabled");
        },
        didInsertElement: function () {
            // hide this element first
            this.$().hide();

            // bind the resizer and the eventhandler
            // after resize.
            this.$().resizable({
                start: $.proxy(this.onResizeStart, this),
                stop: $.proxy(this.onResizeStop, this)
            });

            // bind the dragger and the eventhandler
            // after dragging.
            this.$().draggable({
                start: $.proxy(this.onDragStart, this),
                stop: $.proxy(this.onDragStop, this)
            });

            // load the buttons and their actionbar
            this.set("actionBar", $(".action-bar", this.$()));
            this.set("editButton", $(".btn-edit", this.$()));
            this.set("destroyButton", $(".btn-destroy", this.$()));

            // bind the nesseccary eventhandlers to the actiobar
            // buttons.
            this.editButton.click($.proxy(this.beginEdit, this));
            this.destroyButton.click($.proxy(this.deleteNote, this));

            // fade this element in
            this.$().fadeIn('fast');

        },

        beginEdit: function () {
            var editing = this.controller.get('editMode');
            this.controller.set('editMode', !editing);
            if (editing) {
                this.controller.store.commit();
            }
        },

        deleteNote: function () {
            if (confirm("Sure, delete this note???")) {
                this.controller.deleteRecord();
                this.controller.store.commit();
                this.destroy();
            }
        }

    });

    window.NoteController = Ember.Controller.extend({
        id: null,
        markdownContent: "",
        position: {
            left: 10,
            top: 10
        },
        size: {
            height: 300,
            width: 300
        },
        editMode: false
    });


    window.EditorView = Ember.View.extend({
        templateName: "markdowneditortemplate",
        _editor: null,
        didInsertElement: function () {
            var self = this;
            var aceEditor = ace.edit(this.elementId);
            // aceEditor.setTheme("ace/themes/monokai");
            aceEditor.getSession().setMode("ace/mode/markdown");
            aceEditor.getSession().on('change', function (evt) {
                self.controller.set('markdownContent', aceEditor.getValue());
            });
            this.set('_editor', aceEditor);

        },
        bindNewControllerToEditor: function () {
            this._editor.setValue(this.controller.get('markdownContent'));
        }
    });

    $(function () {

        // bind the eventhandler for creating new notes

        initialNoteData = App.Note.find();

        var interval = setInterval(function () {
            if (initialNoteData.get('isLoaded')) {
                clearInterval(interval);
                for (var i = 0; i < initialNoteData.get('length'); i++) {
                    window.notesRegister = window.notesRegister || {};

                    var noteInstance = initialNoteData.objectAt(i);

                    var noteView = NoteView.create({
                        controller: noteInstance
                    });
                    // noteView.appendTo(".content-col");

                    notesRegister['note' + noteInstance.get('id')] = {
                        view: noteView,
                        model: noteInstance
                    };
                }
            }
        }, 100);


        $("#new-note-button").click(function (evt) {

            var note = App.Note.createRecord({
                markdownContent: '',
                height: 200,
                width: 200,
                left: 100,
                top: 100
            });

            var element = NoteView.create({
                controller: note
            });

            console.log(evt);

            element.appendTo(".content-col");

        });

    });


}(jQuery));