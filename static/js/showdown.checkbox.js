(function () {
    window.Showdown.extensions.checkbox = function (converter) {
        return [
            {
                type: 'lang',
                regex: '\\[([ |x|X])\\]',
                replace: function (match, leadingSlash, tag) {
                    if (leadingSlash.toLowerCase() === 'x') {
                        return '<input type="checkbox" disabled checked>';
                    } else {
                        return '<input type="checkbox" disabled>';
                    }
                }
            }
        ];
    };
}());