# coding: utf-8

import json
from bottle import route, static_file, request
from bottle_sqlite import SQLitePlugin

sqlite_plugin = SQLitePlugin(dbfile="data.sqlite3")


@route('/')
def index():
    return static_file('index.html', 'views', mimetype="text/html")


@route('/static/<filename:path>')
def static(filename):
    return static_file(filename, root='./static/')


@route('/api/notes', apply=[sqlite_plugin])
def api_get_notes(db):
    cursor = db.execute("SELECT id, markdown_content, height, width, top, left FROM notes")
    data = []
    for entry in cursor:
        data.append({
            'id': entry['id'],
            'markdown_content': entry['markdown_content'],
            'top': entry['top'],
            'left': entry['left'],
            'height': entry['height'],
            'width': entry['width'],
        })

    return json.dumps({'notes': data})


@route('/api/notes/<note_id>', method=['GET'], apply=[sqlite_plugin])
def api_get_note(db, note_id):
    cursor = db.execute("SELECT id, markdown_content, height, width, top, left FROM notes WHERE id = ?", (note_id, ))
    entry = cursor.fetchone()

    return json.dumps({
        'note': {
            'id': entry['id'],
            'markdown_content': entry['markdown_content'],
            'top': entry['top'],
            'left': entry['left'],
            'height': entry['height'],
            'width': entry['width'],
        }
    })


@route('/api/notes/<note_id>', method=['PUT'], apply=[sqlite_plugin])
def api_put_note(db, note_id):

    data = request.json['note']

    db.execute("UPDATE notes SET markdown_content = ?, height = ?, width = ?, top = ?, left = ? WHERE id = ?", [
        data['markdown_content'], data['height'], data['width'], data['top'], data['left'], note_id,
    ])

    return api_get_note(db, note_id)


@route('/api/notes', method=['POST'], apply=[sqlite_plugin])
def api_post_note(db):

    data = request.json['note']

    cursor = db.cursor()
    cursor.execute("INSERT INTO notes(markdown_content, height, width, top, left) VALUES (?, ?, ?, ?, ?)", [
        data['markdown_content'], data['height'], data['width'], data['top'], data['left']
    ])

    return api_get_note(db, cursor.lastrowid)


@route('/api/notes/<note_id>', method=['DELETE'], apply=[sqlite_plugin])
def api_delete_note(db, note_id):
    db.execute('DELETE FROM notes WHERE id = ?', [note_id, ])
    return {}


@route('/api/pages')
def api_get_pages():
    return {
        'pages': [
            {'id': 1, 'name': 'Seite 1'},
            {'id': 2, 'name': 'Seite 2'},
        ]
    }


@route('/api/pages/<page_id>')
def api_get_page(page_id):
    return json.dumps({
        'page': {
            'id': page_id,
            'name': 'Seite %s' % page_id,
        }
    })

# run(host="0.0.0.0", port=int(os.environ.get("PORT", 5000)))
